<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('checkmail');
Route::get('/home/gmail', 'HomeController@gmail')->name('gmail')->middleware('checkmail');
Route::get('/home/another', 'HomeController@another')->name('another')->middleware('checkmail');


Route::match(['get', 'post'], 'home/{mailType}/action', ['as' => 'api.statistic', function() {
    $actionController = new \App\Http\Controllers\HomeController();
    $content = $actionController->action();

    //return response()->json($content);
    return $content;
}])->where(['mailType'=>'(gmail|another)'])->middleware('checkmail');

Route::get('/home/another/coll/{id}', 'HomeController@collection')->name('homecoll')->where('id', '[0-9]+')->middleware('checkmail');;


Route::match(['get', 'post'], 'home/gmail/action_add_book', 'HomeController@addBook')
    ->name('addBook')->middleware('checkmail');

Route::match(['get', 'post'], 'home/another/action_add_collection', 'HomeController@addCollection')
    ->name('addCollection')->middleware('checkmail');

Route::match(['get', 'post'], 'home/another/action_add_book_2_collection', 'HomeController@addBook2Collection')
    ->name('addBook2Collection')->middleware('checkmail');

Route::get('home/another/action_get_book', 'HomeController@getBook')
    ->name('getBook')->middleware('checkmail');


