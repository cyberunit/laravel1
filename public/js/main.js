(function($){
    $(document).ready(function(){
        registerChecking();
        bookForm();
        collectForm();
        collectFillForm();
        gotoCollection();
        gotoBack();
    });

    function registerChecking() {
        var inp1 = $('#password');
        var inp2 = $('#password-confirm');
        var btn = $('button[type="submit"]');
        if(!inp1.length || !inp2.length || !btn.length) return;

        btn.attr('disabled', 'disabled');

        function passInputProcess() {
            var pass = inp1.val();
            var pass2 = inp2.val();
            if(pass != pass2 || pass.length<6) {
                btn.attr('disabled', 'disabled');
            }else{
                btn.removeAttr('disabled');
            }
        }
        inp1.on('input',passInputProcess);
        inp2.on('input',passInputProcess);
    }

    function addItem(data) {
        $.ajax({
            url: '/home/'+data.actionType+'/action_'+data.action,
            method: 'GET',
            cache: false,
            data: data,
            dataType: 'json',
            contentType: "application/json",
            success: function(data){
                if(data.status != 'ok') {
                    alert(data.msg);
                    return;
                }
                location.reload();
            }
        });
    }

    function bookForm() {
        var sendBtn = $('input[name="addBook"]');
        var textInp = $('input[name="title"]');
        if(!sendBtn.length || !textInp.length) {
            return;
        }

        sendBtn.on('click', function () {
            var title = textInp.val();
            if(!title) return;

            var data = {
                actionType: 'gmail',
                action: 'add_book',
                title: title
            };
            addItem(data);
        });
    }

    function collectForm() {
        var sendBtn = $('input[name="addCollect"]');
        var textInp = $('input[name="title"]');
        if(!sendBtn.length || !textInp.length) {
            return;
        }

        sendBtn.on('click', function () {
            var title = textInp.val();
            if(!title) return;

            var data = {
                actionType: 'another',
                action: 'add_collection',
                title: title
            };
            addItem(data);
        });
    }

    function collectFillForm() {
        var sendBtn = $('input[name="addCollBook"]');
        var textInp = $('input[name="title"]');
        if(!sendBtn.length || !textInp.length) {
            return;
        }

        var options = {
            url: function(phrase) {
                return "/home/another/action_get_book?title=" + phrase;
            },
            getValue: function(element) {
                return element.title+' ['+element.book_id+']';
            },
            listLocation: "list",
            requestDelay: 500
        };

        textInp.easyAutocomplete(options);


        sendBtn.on('click', function () {
            var title = textInp.val();
            if(!title) return;
            var match = /.*\[(\d+)\]/.exec(title);
            //console.log('!!!', match, title);
            var bookId = match[1] ? match[1]: 0;
            if(!bookId) return;
            var collId = $('#goToBack').attr('data-coll-id');

            var data = {
                actionType: 'another',
                action: 'add_book_2_collection',
                book_id: bookId,
                coll_id: collId
            };
            addItem(data);
        });

    }


    function gotoCollection() {
        $('.panel-body table tr').on('click',function (e) {
            var tag = $(this);
            var id = tag.attr('data-id');
            if(!(id>0)) return;
            window.location = '/home/another/coll/'+id;
            return false;
        })
    }

    function gotoBack() {
        $('#goToBack').on('click', function () {
            window.location = '/home';
            return false;
        })
    }

})(jQuery);