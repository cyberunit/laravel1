<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Collect;
use App\Models\CollectContent;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\View;

/**
 *
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @var \App\User
     */
    private $user;
    private $domain;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
           //$this->middleware('auth'); //не редиректит он ни фига, хоть и должен по инструкции

    }


    public function initData() { //не нашел в ларавеле функции beforeAction, поэтому вызываю эту фиговню везде
        $this->user = \Request::get('siteUser', '');
        $this->domain = \Request::get('domain', '');
        //dd(['user'=>$this->user]);
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->initData();
        return view('home', []);
    }


    /**
     * Home for another mail
     */
    public function another() {
        $this->initData();
        $collList = $this->user->getUserCollects();
        return view('home_another', ['user'=>$this->user, 'list'=>$collList]);
    }



    /**
     * Home for @gmail.com
     */
    public function gmail() {
        $this->initData();
        $bookList = $this->user->getUserBooks();

        return view('home_gmail', ['user'=>$this->user, 'list'=>$bookList]);
    }


    public function collection($id) {
        $this->initData();

        $coll = Collect::find((int)$id);
        if($coll) {
            $coll_content = $coll->getBooks();
        }else{
            return view('coll_wrong' , [ 'user'=>$this->user ]);
        }
        return view('coll_another', ['coll_id'=>$id, 'coll'=>$coll, 'list'=>$coll_content, 'user'=>$this->user, ]);

    }




    public function action(Request $request) {
        $action = $request->get('action');
        switch ($action){
            case 'add_book':
                return $this->addBook();
            case 'add_collection':
                return $this->addCollection();
            case 'add_book_2_collection':
                return $this->addBook2Collection();
            case 'get_book':
                return $this->getBook();
        }
        return ['status'=>'error', 'msg'=>'Action not found'];
    }


    public function addBook(Request $request) {
        $this->initData();
        if($this->domain != 'gmail.com') {
            return ['status'=>'error', 'msg'=>'You can\'t do it'];
        }
        $title = trim($request->get('title'));
        if(!$title) return ['status'=>'error', 'msg'=>'Book title is absent'];

        $book = new Book();
        $book->title = $title;
        $book->uid = $this->user->id;
        $book->save();
        //$book->getIncrementing();
        $res = ['status'=>'ok', 'id'=>$book->id, ];
        return response()->json($res);
    }


    public function addCollection(Request $request) {
        $this->initData();
        if($this->domain == 'gmail.com') {
            return ['status'=>'error', 'msg'=>'You can\'t do it'];
        }

        $title = trim($request->get('title'));
        if(!$title) return ['status'=>'error', 'msg'=>'Collection title is absent'];

        $colection = new Collect();
        $colection->title = $title;
        $colection->uid = $this->user->id;
        $colection->save();
        $res = ['status'=>'ok', 'id'=>$colection->id];
        return response()->json($res);
    }


    public function addBook2Collection(Request $request) {
        $this->initData();
        if($this->domain == 'gmail.com') {
            return ['status'=>'error', 'msg'=>'You can\'t do it'];
        }

        $bookId = (int) $request->get('book_id');
        $collId = (int) $request->get('coll_id');
        if(!$bookId or !$collId) {
            return ['status'=>'error', 'msg'=>'Unsufficient data'];
        }

        $bookLink = new CollectContent();
        $bookLink->col_id = $collId;
        $bookLink->book_id = $bookId;
        $bookLink->save();

        $res = ['status'=>'ok', 'id'=>$bookLink->id];
        return response()->json($res);
    }


    public function getBook(Request $request) {
        $title = trim($request->get('title'));
        $list = Book::byTitle($title);

        $res = ['status'=>'ok', 'list'=>$list];
        return response()->json($res);
    }

}
