<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckMail
{

    private $user;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $domain = $this->checkUser();
        if (!$domain) { // Пользователь не вошёл в систему...
            return redirect('login');
        }
        if($domain == 'gmail.com') {
            $checkPath = '/home/gmail';
        }else{
            $checkPath = '/home/another';
        }
        if($checkPath and substr($request->getRequestUri(),0, strlen($checkPath))!=$checkPath) {
            return redirect(substr($checkPath, 1));
        }

        $request->attributes->add([ 'siteUser' => $this->user, 'domain'=>$domain, ]);
        $response = $next($request);
        return $response;
    }


    /**
     * проверка юзверя
     *
     * @return $domain домен почты
     */
    private function checkUser() {
        if (!Auth::check()) { // Пользователь не вошёл в систему...
            return '';
        }
        $this->user = Auth::user();
        $emailParts = explode('@', $this->user->email);
        return array_pop($emailParts);
    }




}
