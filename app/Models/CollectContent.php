<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollectContent extends Model
{

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'collect_content';


//    public static function byColId($cid) {
//        $q = self::from('collect_content as cc')->where('col_id', $cid);
//        $q->join('book as b', 'b.id', '=', 'cc.book_id');
//        $q->select(['cc.col_id', 'cc.book_id', 'b.title']);
//        $res = $q->get();
//        $list = [];
//        foreach ($res as $item) {
//            $list[$item->book_id] = $item;
//        }
//        return $list;
//    }
}
