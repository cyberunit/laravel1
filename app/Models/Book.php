<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Book extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'book';


    public static function byTitle($title) {
        $res = self::where('title', 'like', $title. '%')->select(['id', 'title'])->get();
        //DB::enableQueryLog();
        //dd(DB::getQueryLog());
        $list = [];
        foreach ($res as $item) {
            $list[] = [ 'book_id'=>$item->id, 'title'=>$item->title ];
        }
        return $list;
    }
}
