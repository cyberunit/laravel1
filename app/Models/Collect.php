<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collect extends Model
{

    /**
    * Связанная с моделью таблица.
    *
    * @var string
    */
    protected $table = 'collect';

    public function books() {
        return $this->belongsToMany('App\Models\Book', 'collect_content', 'col_id', 'book_id', 'id', 'id');
    }


    public function getBooks() {
        $res = $this->books()->select(['collect_content.col_id', 'collect_content.book_id', 'book.title'])->get();
        $list = [];
        foreach ($res as $item) {
            $list[$item->book_id] = $item;
        }
        return $list;
    }

}
