<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function books() {
        return $this->hasMany('App\Models\Book', 'uid');
    }


    public function collects() {
        return $this->hasMany('App\Models\Collect', 'uid');
    }


    public function getUserBooks() {
        $res = $this->books()->select([ 'id', 'title', ])->get();
        $list = [];
        foreach ($res as $item) {
            $list[$item->id] = $item->title;
        }
        return $list;
    }

    public function getUserCollects() {
        $res = $this->collects()->select([ 'id', 'title', ])->get();
        $list = [];
        foreach ($res as $item) {
            $list[$item->id] = $item->title;
        }
        return $list;
    }



}
