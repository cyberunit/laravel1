@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    {{ $user->email }}
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><input name="title" style="width: 70%" /> <input type="button" name="addCollect" value="Add new collection" /></div>

                <div class="panel-body">
                    @if(count($list))
                    <table>
                        <thead><tr><th>id</th><th>title</th></tr></thead>
                        <tbody>
                        @foreach ($list as $id=>$title)
                        <tr data-id="{{ $id }}" style="cursor: pointer;"><td>{{ $id }}</td><td>{{ $title }}</td></tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
